import cx_Oracle
import json
import os
import requests
import time
from datetime import date, timedelta

from airflow import DAG, utils
from airflow.operators.python_operator import PythonOperator

from oracle_connect import *

LOGIN = 'vivakiagency'
TOKEN = 'AQAAAAARsustAASegoTjTylKCkFmpzEtqUOFEMg'
CLIENT_LOGIN = 'prgr-samsung'

class Oracle:

    def connect(self, username, password, hostname, port, servicename):
        """ Connect to the database. """

        try:
            os.environ["NLS_LANG"] = "RUSSIAN_CIS.CL8MSWIN1251"
            connect_string='{}/{}@(DESCRIPTION = (ADDRESS = (PROTOCOL = TCP)(HOST = {})(PORT = {}))(CONNECT_DATA =(SERVER = DEDICATED)(SERVICE_NAME = {})))'.format(username, password, hostname, port, servicename)
            print(connect_string)
            self.db = cx_Oracle.connect(connect_string)
        except cx_Oracle.DatabaseError as e:
            raise

        self.cursor = self.db.cursor()

    def disconnect(self):
        """
        Disconnect from the database. If this fails, for instance
        if the connection instance doesn't exist, ignore the exception.
        """

        try:
            self.cursor.close()
            self.db.close()
        except cx_Oracle.DatabaseError:
            pass

    def execute(self, sql, bindvars=None, commit=False, is_return=False):
        """
        Execute whatever SQL statements are passed to the method;
        commit if specified. Do not specify fetchall() in here as
        the SQL statement may not be a select.
        bindvars is a dictionary of variables you pass to execute.
        """

        try:
            result = self.cursor.execute(sql)
        except cx_Oracle.DatabaseError as e:
            # Log error as appropriate
            raise

        if commit:
            self.db.commit()
        if is_return:
            return result

class DataLoaderToOracle:
    def __init__(self, table_name):
        self.table_name = table_name
        self.oracle = Oracle()
        self.oracle.connect('tableau', 'VztQIh', '192.168.166.50', '1521', 'rupmed01.global.publicisgroupe.net')

    def create(self, table_cols):
        self.temp_table_name = '{}_{}'.format(self.table_name, str(int(time.time())))
        ct = "CREATE TABLE {} ({})".format(self.temp_table_name, ", ".join([col[0] + ' ' + col[1] for col in table_cols]))
        self.oracle.execute(ct)
        self.table_cols = table_cols
		
    def select(self, text):
        selected = self.oracle.execute(text, is_return=True)
        return selected.fetchall()

    def insert(self, item):
        insert_str = "INSERT INTO {} ({}) VALUES (".format(self.temp_table_name, ", ".join([col[0] for col in self.table_cols]))
        for col in self.table_cols:
            if 'VARCHAR' in col[1]:
                if not item[col[0].lower()]:
                    item[col[0].lower()] = ''
                insert_str += " '{}',".format(str(item[col[0].lower()]).replace("'", "''"))
            elif 'DATE' in col[1]:
                if item[col[0].lower()] is None:
                    insert_str += " NULL,"
                else:
                    insert_str += " TO_CHAR('{}'),".format(datetime.strftime(datetime.strptime(item[col[0].lower()], '%Y-%m-%d'), '%d.%m.%Y'))
            else:
                if not item[col[0].lower()]:
                    item[col[0].lower()] = 'NULL'
                insert_str += " {},".format(item[col[0].lower()] if item[col[0].lower()] else None)
        insert_str = insert_str[:-1] + ")"
        print(insert_str)
        self.oracle.execute(insert_str.encode('utf-8'), commit=True)

    def merge(self, merge_cols=1):
        merge_str = r"MERGE INTO {}_AIRFLOW va USING (select * from {}) v1 ON (va.{} = v1.{}".format(self.table_name.upper(), self.temp_table_name, \
            self.table_cols[0][0], self.table_cols[0][0])
        for i in range(1, merge_cols):
            merge_str += " AND va.{} = v1.{}".format(self.table_cols[i][0], self.table_cols[i][0])
        merge_str += ") WHEN MATCHED THEN UPDATE SET"
        for col in self.table_cols[merge_cols:]:
            merge_str += " va.{}=v1.{},".format(col[0], col[0])
        merge_str = merge_str[:-1] + " WHEN NOT MATCHED THEN INSERT ("
        for col in self.table_cols:
            merge_str += "va.{}, ".format(col[0])
        merge_str = merge_str[:-2] + ") VALUES ("
        for col in self.table_cols:
            merge_str += "v1.{}, ".format(col[0])
        merge_str = merge_str[:-2] + ')'
        print(merge_str)
        self.oracle.execute(merge_str, commit=True)
        self.drop_temp_table()

    def drop_temp_table(self):
        self.oracle.execute("DROP TABLE " + self.temp_table_name, commit=True)
        self.oracle.disconnect()


class YandexDirect:

    def __init__(self, direct_login, direct_token):
        self.app_id = '71e6cc7667f140d3878d7565eba4ca89'
        self.direct_token = direct_token
        self.url = 'https://api.direct.yandex.com/json/v5/'
        self.direct_login = direct_login

        self.headers = {
            # OAuth-токен. Использование слова Bearer обязательно
            "Authorization": "Bearer " + self.direct_token,
            # Логин клиента рекламного агентства
            "Client-Login": self.direct_login,
            # Язык ответных сообщений
            "Accept-Language": "ru",
        }
        self.data = {
            "method": "get",
            "params": {
                "SelectionCriteria": {},
                "FieldNames": []
            }
        }

    def get_clients(self):
        # Agency Clients
        url = self.url + 'agencyclients'
        self.data["params"]["FieldNames"] = ['ClientId', 'ClientInfo', 'Login', 'Currency']
        json_data = json.dumps(self.data, ensure_ascii=False).encode('utf8')
        response = requests.post(url=url, data=json_data, headers=self.headers)
        return response.json()['result']['Clients']

    def get_client(self):
        # Clients
        url = self.url + 'clients'
        self.data["params"]["FieldNames"] = ['ClientId', 'ClientInfo', 'Login', 'Currency']
        params = {"method": "get",
                  "params": {
                      "FieldNames": ['ClientId', 'ClientInfo', 'Login', 'Currency']
                  }
                  }
        json_data = json.dumps(params, ensure_ascii=False).encode('utf8')
        response = requests.post(url=url, data=json_data, headers=self.headers)
        print(response.json())
        return response.json()

    def get_campaigns(self):
        url = self.url + 'campaigns'
        self.data["params"]["FieldNames"] = ['Id', 'Name', 'Status', 'Statistics', 'StartDate', 'EndDate',
                                             'DailyBudget', 'Currency', 'ClientInfo', 'StatusPayment', 'Type']
        self.headers['Client-Login'] = CLIENT_LOGIN

        json_data = json.dumps(self.data, ensure_ascii=False).encode('utf8')
        response = requests.post(url=url, data=json_data, headers=self.headers)
        return response.json()

    def get_ads(self, login, campaign_ids):
        url = self.url + 'ads'
        self.data['params']['SelectionCriteria']['CampaignIds'] = campaign_ids
        self.data["params"]["FieldNames"] = [
            "AdCategories", "AgeLabel", "AdGroupId", "CampaignId",
            "Id", "State", "Status", "StatusClarification", "Type",
            "Subtype"
        ]
        self.data['params']['TextAdFieldNames'] = [
            'Href', 'DisplayUrlPath',
            'DisplayUrlPathModeration',
            'SitelinkSetId', 'SitelinksModeration'
        ]
        self.data['params']['CpmBannerAdBuilderAdFieldNames'] = ['Creative', 'Href', 'TrackingPixels']
        self.data['params']['CpcVideoAdBuilderAdFieldNames'] = ['Creative', 'Href']
        self.data['params']['MobileAppAdBuilderAdFieldNames'] = ['Creative', 'TrackingUrl']
        self.data['params']['TextAdBuilderAdFieldNames'] = ['Creative', 'Href']
        self.headers['Client-Login'] = login
        json_data = json.dumps(self.data, ensure_ascii=False).encode('utf8')
        response = requests.post(url=url, data=json_data, headers=self.headers)
        return response.json()

    def get_adgroups(self, login, campaign_ids):
        url = self.url + 'adgroups'
        self.data['params']['SelectionCriteria']['CampaignIds'] = campaign_ids
        self.data["params"]["FieldNames"] = [
            "CampaignId", "Id", "Name", "NegativeKeywords", "RegionIds",
            "RestrictedRegionIds", "ServingStatus", "Status", "Subtype",
            "TrackingParams", "Type"
        ]
        self.headers['Client-Login'] = login

        json_data = json.dumps(self.data, ensure_ascii=False).encode('utf8')
        response = requests.post(url=url, data=json_data, headers=self.headers)
        return response.json()


    def get_task_from_queue(self, url, body, headers):
        body = json.dumps(body, indent=4)
        while True:
            try:
                req = requests.post(url, body, headers=headers)
                print("RequestId: {}".format(req.headers.get("RequestId", False)))
                if req.status_code == 400:
                    print("Параметры запроса указаны неверно или достигнут лимит отчетов в очереди")
                    print("JSON-код запроса: {}".format(body))
                    print("JSON-код ответа сервера: \n{}".format(req.json()))
                    break
                elif req.status_code == 500:
                    print("При формировании отчета произошла ошибка. Пожалуйста, попробуйте повторить запрос позднее")
                    print("JSON-код ответа сервера: \n{}".format(u(req.json())))
                    break
                elif req.status_code == 201:
                    print("Отчет успешно поставлен в очередь в режиме офлайн")
                    retryIn = int(req.headers.get("retryIn", 60))
                    print("Повторная отправка запроса через {} секунд".format(retryIn))
                    time.sleep(retryIn)
                elif req.status_code == 202:
                    print("Отчет формируется в режиме офлайн")
                    retryIn = int(req.headers.get("retryIn", 60))
                    print("Повторная отправка запроса через {} секунд".format(retryIn))
                    time.sleep(retryIn)
                elif req.status_code == 200:
                    print("Отчет создан успешно")
                    if req.text != "":
                        return req.text.split('\n')
                    break
                else:
                    print("Произошла непредвиденная ошибка")
                    print("JSON-код запроса: {}".format(body))
                    print("JSON-код ответа сервера: \n{}".format(req.json()))
                    break
            except Exception as e:
                print("Произошла непредвиденная ошибка {}".format(e))
                break

    def get_stat_by_campaign(self, direct_client_login, start_date=date.today()-timedelta(days=5), end_date=date.today(), vat='NO', discount='NO'):
        
        self.headers['skipReportHeader'] = "true"
        self.headers['skipColumnHeader'] = "false"
        self.headers['skipReportSummary'] = "true"
        self.headers["processingMode"] = "auto"
        url = self.url + 'reports'
        # "AdGroupId", "AdId"
        body = {
            "params": {
                "SelectionCriteria": {
                    "DateFrom": str(start_date),
                    "DateTo": str(end_date)
                },
                "FieldNames": [
                    "Date",
                    "CampaignType",
                    "CampaignId",
                    "CampaignName",
                    "Impressions",
                    "Clicks",
                    "Cost",
                    "CostPerConversion",
                    "Conversions",
                    "Bounces",
                    "AvgClickPosition",
                    "AvgImpressionPosition",
                    "GoalsRoi",
                    "Revenue",
                    "Sessions",
                    "ImpressionShare"
                ],
                "ReportName": "PERFORMANCE_REPORT",
                "ReportType": "CAMPAIGN_PERFORMANCE_REPORT",
                "DateRangeType": "CUSTOM_DATE",
                "Format": "TSV",
                "IncludeVAT": "NO",
                "IncludeDiscount": "NO"
            }
        }
        
        
        data = self.get_task_from_queue(url, body, self.headers)
        
        return data

    def stat_reach(self, login, vat='NO', discount='NO', sd=date.today()-timedelta(days=5), ed=date.today()):

        self.headers['Client-Login'] = login
        self.headers['skipReportHeader'] = "true"
        self.headers['skipColumnHeader'] = "false"
        self.headers['skipReportSummary'] = "true"
        self.headers["processingMode"] = "auto"
        url = self.url + 'reports'
        # "AdGroupId", AdId",
        body = {
            "params": {
                "SelectionCriteria": {
                    "DateFrom": str(sd),
                    "DateTo": str(ed)
                },
                "FieldNames": [
                    "CampaignType",
                    "CampaignId",
                    "CampaignName",
                    "ImpressionReach"
                ],
                "ReportName": "REACH_AND_FREQUENCY_PERFORMANCE_REPORT{0}".format(str(ed)),
                "ReportType": "REACH_AND_FREQUENCY_PERFORMANCE_REPORT",
                "DateRangeType": "CUSTOM_DATE",
                "Format": "TSV",
                "IncludeVAT": vat,
                "IncludeDiscount": discount
            }
        }
        data = self.get_task_from_queue(url, body, self.headers)
        return data
    
    def get_stat_by_ads(self, login, vat='NO', discount='NO', sd=date.today()-timedelta(days=5), ed=date.today()):
        self.headers['Client-Login'] = login
        self.headers['skipReportHeader'] = "true"
        self.headers['skipColumnHeader'] = "false"
        self.headers['skipReportSummary'] = "true"
        self.headers["processingMode"] = "auto"
        url = self.url + 'reports'
        # "AdGroupId", "AdId",
        body = {
            "params": {
                "SelectionCriteria": {
                    "DateFrom": str(sd),
                    "DateTo": str(ed)
                },
                "FieldNames": [
                    "Date",
                    "CampaignId", 
                    "AdGroupId", 
                    "AdId",
                    "Impressions",
                    "Clicks",
                    "Cost",
                    "TargetingLocationId",
                    "LocationOfPresenceId"
        
                ],
                "ReportName": "PERFORMANCE_AD_{0}".format(login),
                "ReportType": "AD_PERFORMANCE_REPORT",
                "DateRangeType": "CUSTOM_DATE",
                "Format": "TSV",
                "IncludeVAT": vat,
                "IncludeDiscount": discount
            }
        }
        data = self.get_task_from_queue(url, body, self.headers)
        return data

d = YandexDirect(direct_login=LOGIN, direct_token=TOKEN)

def ya_get_clients(ds, **kwargs):
    clients = d.get_clients()
    dl = DataLoaderToOracle("YDIR_CLIENTS")
    dl.create((("CLIENTID", "VARCHAR2(255 CHAR)"), ("LOGIN", "VARCHAR2(255 CHAR)"), ("CLIENTINFO", "VARCHAR2(255 CHAR)"), ("DIRECTLOGIN", "VARCHAR2(255 CHAR)"), ("CURRENCY", "VARCHAR2(20 BYTE)")))
    for client in clients:
        client_data = {}
        for c in client:
            client_data[c.lower()] = client[c]
        client_data['directlogin'] = LOGIN
        dl.insert(client_data)
    dl.merge()
    return clients


def ya_get_client(ds, **kwargs):
    clients = d.get_client()
    print(clients)
    return clients


def ya_get_campaigns(ds, **kwargs):
    campaigns = d.get_campaigns()
    def get_campaigns_dict(row):
        return {
        'id': row['Id'],
        'client': row['ClientInfo'],
		'status': row.get('Status', None),
        'currency': row.get('Currency', None),
        'type': row.get('Type', None),
		'dailybudget': row.get('DailyBudget', None),
        'name': row.get('Name', None),
        'startdate': row.get('StartDate', None),
        'enddate': row.get('EndDate', None),
        'statuspayment': row.get('StatusPayment', None),
        'clicks': row['Statistics']['Clicks'],
        'impressions': row['Statistics']['Impressions'],
        }
    print(campaigns)
    dl = DataLoaderToOracle("YDIR_CAMPAIGNS")
    dl.create((('ID', 'NUMBER(38,0)'), ("CLIENT", "VARCHAR2(255 CHAR)"), ("STATUS", "VARCHAR2(255 CHAR)"), ("CURRENCY", "VARCHAR2(255 CHAR)"), ("TYPE", "VARCHAR2(255 CHAR)"), \
    ("DAILYBUDGET", "VARCHAR2(255 CHAR)"), ("NAME", "VARCHAR2(4000 CHAR)"), ("STARTDATE", "VARCHAR2(255 CHAR)"), ("ENDDATE", "VARCHAR2(255 CHAR)"), \
    ("STATUSPAYMENT", "VARCHAR2(255 CHAR)"), ('CLICKS', 'NUMBER(38,0)'), ("IMPRESSIONS", 'NUMBER(38,0)')))
    for campaign in campaigns['result']['Campaigns']:
        dl.insert(get_campaigns_dict(campaign))
    dl.merge()
    return campaigns['result']['Campaigns']

def ya_get_ads(ds, **kwargs):
    def get_ads_dict(row):
        return {
        'id': row['Id'],
        'adgroupid': row['AdGroupId'],
        'campaignid': row['CampaignId'],
        'type': row.get('Type', None),
        'state': row.get('State', None),
        'statusclarification': row.get('StatusClarification', None),
        'status': row.get('Status', None),
        'adcategories': row.get('AdCategories', None),
        'agelabel': row.get('AgeLabel', None),
        'subtype': row.get('Subtype', None),
        'href': row['CpmBannerAdBuilderAd'].get('href', None),
        }
    ti = kwargs['ti']
    campaigns = [c['Id'] for c in ti.xcom_pull(task_ids='campaigns')]
    all_ads = []
    for i in range(0, len(campaigns), 10):
        all_ads.append(d.get_ads(CLIENT_LOGIN, campaigns[i:i+10]))
    dl = DataLoaderToOracle("YDIR_ADS")
    dl.create((('ID', 'NUMBER(38,0)'), ('ADGROUPID', 'NUMBER(38,0)'), ('CAMPAIGNID', 'NUMBER(38,0)'), ("TYPE", "VARCHAR2(255 CHAR)"), \
    ("STATE", "VARCHAR2(500 CHAR)"), ("STATUSCLARIFICATION", "VARCHAR2(1000 CHAR)"), ("STATUS", "VARCHAR2(500 CHAR)"), \
    ("ADCATEGORIES", "VARCHAR2(255 CHAR)"), ("AGELABEL", "VARCHAR2(255 CHAR)"), ("SUBTYPE", "VARCHAR2(255 CHAR)"), ("HREF", "VARCHAR2(2000 CHAR)")))
    for ads in all_ads:
        print(ads)
        for ad in ads['result']['Ads']:
            dl.insert(get_ads_dict(ad))
    dl.merge()
    return ads

def ya_get_adgroups(ds, **kwargs):
    def get_adgroups_dict(row):
        return {
        'id': row['Id'],
        'campaignid': row['CampaignId'],
        'name': row.get('Name', None),
        'trackingparams': row.get('TrackingParams', None),
        'servingstatus': row.get('ServingStatus', None),
        'subtype': row.get('Subtype', None),
        'type': row.get('Type', None),
        'status': row.get('Status', None),
        }
    ti = kwargs['ti']
    campaigns = [c['Id'] for c in ti.xcom_pull(task_ids='campaigns')]
    adgroups = []
    for i in range(0, len(campaigns), 10):
        adgroups.extend(d.get_adgroups(CLIENT_LOGIN, campaigns[i:i+10])['result']['AdGroups'])
    dl = DataLoaderToOracle("YDIR_ADGROUPS")
    dl.create((('ID', 'NUMBER(38,0)'), ('CAMPAIGNID', 'NUMBER(38,0)'), ("NAME", "VARCHAR2(500 CHAR)"), \
    ("TRACKINGPARAMS", "VARCHAR2(800 CHAR)"), ("SERVINGSTATUS", "VARCHAR2(800 CHAR)"), ("SUBTYPE", "VARCHAR2(800 CHAR)"), \
    ("TYPE", "VARCHAR2(800 CHAR)"), ("STATUS", "VARCHAR2(800 CHAR)")))
    for group in adgroups:
        dl.insert(get_adgroups_dict(group))
    dl.merge()
    return adgroups

def ya_get_stat_by_campaign(ds, **kwargs):
    #dl = DataLoaderToOracle("YDIR_STAT_BY_AD")
    logins = dl.select("select login, token from YDIR_MAIN where type=0")
    for login, token in logins:
        print(login, token)
        yandex_client = YandexDirect(login, token)
        stat = yandex_client.get_stat_by_campaign(login)
        print(stat)
        stat = yandex_client.stat_reach(login)
        print(stat)
    return stat

def ya_get_stat_by_ads(ds, **kwargs):
    ti = kwargs['ti']
    clients = ti.xcom_pull(task_ids='clients')
    for client_info in clients:
        if client_info['Currency'] == 'YND_FIXED':
            vat="NO"
        else:
            vat="YES"
        stat = d.get_stat_by_ads(client_info['Login'], vat=vat)
        print(stat)
    return stat
    


default_args = {
    'owner': 'airflow',
    'depends_on_past': False,
    'start_date': utils.dates.days_ago(2),
	'provide_context': True,
	}

dag = DAG('yandex_direct_api', default_args=default_args)


ya_clients = PythonOperator(
   task_id='clients',
   python_callable=ya_get_clients,
   provide_context=True,
   dag=dag
)

ya_client = PythonOperator(
   task_id='client',
   python_callable=ya_get_client,
   provide_context=True,
   dag=dag
)

ya_campaigns = PythonOperator(
   task_id='campaigns',
   python_callable=ya_get_campaigns,
   provide_context=True,
   dag=dag
)

ya_ads = PythonOperator(
   task_id='ads',
   python_callable=ya_get_ads,
   provide_context=True,
   dag=dag
)

ya_adgroups = PythonOperator(
   task_id='adgroups',
   python_callable=ya_get_adgroups,
   provide_context=True,
   dag=dag
)

ya_campaign_stat = PythonOperator(
   task_id='campaign_stat',
   python_callable=ya_get_stat_by_campaign,
   provide_context=True,
   dag=dag
)

ya_ads_stat = PythonOperator(
   task_id='ad_stat',
   python_callable=ya_get_stat_by_ads,
   provide_context=True,
   dag=dag
)



ya_campaign_stat << ya_ads_stat << ya_adgroups << ya_ads << ya_campaigns << ya_client << ya_clients