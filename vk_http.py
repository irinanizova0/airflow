#-*- coding: utf-8 -*-

import csv
import cx_Oracle
import time
from airflow import DAG, utils
from airflow.operators.dummy_operator import DummyOperator
from airflow.operators.python_operator import PythonOperator
from datetime import datetime, timedelta
import os

#from constants import START_DATE, END_DATE
#from constants import START_DATE, END_DATE

from vk_api import VkApi, AuthError, ApiError

START_DATE = None
END_DATE = None


def auth():
    token = 'b138338016dda89e0c5750b0d88f64e8ef97d6af442d40cea631bc7f11b09dfaaa93ec9675aaac32e2654'
    app_id = 6221956
    login = 'victor.ermakov@publicismedia.ru'
    password = 'pubmedrus'
    vk_session = VkApi(login=login, password=password, token=token, app_id=app_id)
    try:
        vk_session.auth(token_only=True)
    except AuthError as error_msg:
        logger.error(f'Auth error with error:  \n\t{error_msg}')
        return
    api = vk_session.get_api()
    return api


class Oracle:

    def connect(self, username, password, hostname, port, servicename):
        """ Connect to the database. """

        try:
            os.environ["NLS_LANG"] = "RUSSIAN_CIS.CL8MSWIN1251"
            connect_string='{}/{}@(DESCRIPTION = (ADDRESS = (PROTOCOL = TCP)(HOST = {})(PORT = {}))(CONNECT_DATA =(SERVER = DEDICATED)(SERVICE_NAME = {})))'.format(username, password, hostname, port, servicename)
            print(connect_string)
            self.db = cx_Oracle.connect(connect_string)
        except cx_Oracle.DatabaseError as e:
            raise

        self.cursor = self.db.cursor()

    def disconnect(self):
        """
        Disconnect from the database. If this fails, for instance
        if the connection instance doesn't exist, ignore the exception.
        """

        try:
            self.cursor.close()
            self.db.close()
        except cx_Oracle.DatabaseError:
            pass

    def execute(self, sql, bindvars=None, commit=False):
        """
        Execute whatever SQL statements are passed to the method;
        commit if specified. Do not specify fetchall() in here as
        the SQL statement may not be a select.
        bindvars is a dictionary of variables you pass to execute.
        """

        try:
            self.cursor.execute(sql)
        except cx_Oracle.DatabaseError as e:
            # Log error as appropriate
            raise

        if commit:
            self.db.commit()
			
def write_and_merge_info(table_name, table_cols, data, merge_cols=1):
    try:
        oracle = Oracle()
        oracle.connect('tableau', 'VztQIh', '192.168.166.50', '1521', 'rupmed01.global.publicisgroupe.net')
        temp_table_name = '{}_{}'.format(table_name, str(int(time.time())))
        ct = "CREATE TABLE {}_AIRFLOW ({})".format(temp_table_name, ", ".join([col[0] + ' ' + col[1] for col in table_cols]))
        print(ct)
        oracle.execute(ct)
        for item in data:
            insert_str = "INSERT INTO {} ({}) VALUES (".format(temp_table_name, ", ".join([col[0] for col in table_cols]))
            for col in table_cols:
                if 'VARCHAR' in col[1]:
                    if not item[col[0].lower()]:
                        item[col[0].lower()] = ''
                    insert_str += " '{}',".format(str(item[col[0].lower()]).replace("'", "''").replace("é", "e").replace('й', 'й').replace('ё', 'ё').replace('₽','руб.')
                    .replace('―', '-').replace('É', 'E').replace('×', '*').replace('è', 'e').replace('ö', 'o').replace('́', ''))
                    import re
                    smiles = re.findall('[\u2600-\u27ff]', insert_str)
                    smiles += re.findall('[\U0001F300-\U0001F5FF\U0001F600-\U0001F64F\U0001F680-\U0001F6FF]', insert_str)
                    for smile in smiles:
                        insert_str = insert_str.replace(smile, '')
                elif 'DATE' in col[1]:
                    if item[col[0].lower()] is None:
                        insert_str += " NULL,"
                    else:
                        insert_str += " TO_CHAR('{}'),".format(datetime.strftime(datetime.strptime(item[col[0].lower()], '%Y-%m-%d'), '%d.%m.%Y'))
                else:
                    if not item[col[0].lower()]:
                        item[col[0].lower()] = 'NULL'
                    insert_str += " {},".format(item[col[0].lower()] if item[col[0].lower()] else None)
            insert_str = insert_str[:-1] + ")"
            print(insert_str)
            oracle.execute(insert_str, commit=True)
        merge_str = r"MERGE INTO {} va USING (select * from {}) v1 ON (va.{} = v1.{}".format(table_name.upper(), temp_table_name, table_cols[0][0], table_cols[0][0])
        for i in range(1, merge_cols):
            merge_str += " AND va.{} = v1.{}".format(table_cols[i][0], table_cols[i][0])
        merge_str += ") WHEN MATCHED THEN UPDATE SET"
        for col in table_cols[merge_cols:]:
            merge_str += " va.{}=v1.{},".format(col[0], col[0])
        merge_str = merge_str[:-1] + " WHEN NOT MATCHED THEN INSERT ("
        for col in table_cols:
            merge_str += "va.{}, ".format(col[0])
        merge_str = merge_str[:-2] + ") VALUES ("
        for col in table_cols:
            merge_str += "v1.{}, ".format(col[0])
        merge_str = merge_str[:-2] + ')'
        print(merge_str)
        oracle.execute(merge_str, commit=True)
    except cx_Oracle.DatabaseError as e:
        print(e)
        raise cx_Oracle.DatabaseError
    finally:
        oracle.execute("DROP TABLE " + temp_table_name, commit=True)
        oracle.disconnect()


def vk_get_acounts(ds, **kwargs):
    api = auth()
    accounts = api.ads.getAccounts()
    write_and_merge_info('vk_accounts', (('ACCOUNT_ID', 'NUMBER(38,0)'), ('ACCOUNT_TYPE', 'VARCHAR2(25 CHAR)'), ('ACCOUNT_STATUS', 'NUMBER(38,0)'),  \
	    ('ACCESS_ROLE', 'VARCHAR2(25 CHAR)'), ('ACCOUNT_NAME', 'VARCHAR2(225 CHAR)')), accounts)
    return accounts
   
 
def vk_get_clients(ds, **kwargs):
    api = auth()
    ti = kwargs['ti']
    accounts = ti.xcom_pull(task_ids='acount')
    clients = []
    for acc in accounts:
        try:
            client = api.ads.getClients(account_id=acc['account_id'])
            accounts = ti.xcom_push(key = str(acc['account_id']), value=client)
            [c.update({'account_id':acc['account_id']}) for c in client]
            clients.extend(client)
            print(client)
        except ApiError:
            accounts = ti.xcom_push(key = str(acc['account_id']), value='')
            print(acc['account_id'])
    write_and_merge_info('vk_clients', (('ID', 'NUMBER(38,0)'), ('NAME', 'VARCHAR2(150 CHAR)'),  \
	    ('DAY_LIMIT', 'VARCHAR2(200 CHAR)'), ('ALL_LIMIT', 'VARCHAR2(200 CHAR)'), ('ACCOUNT_ID', 'NUMBER(38,0)')), clients)
    return clients
    

def vk_get_ads(ds, **kwargs):
    ti = kwargs['ti']
    accounts = ti.xcom_pull(task_ids='acount')
    api = auth()
    adss = []
    def get_ads_dict(row):
        return {
                        'id' : row.get('id'),
                        'campaign_id' : row.get('campaign_id', None),
                        'ad_format' : row.get('ad_format', None),
                        'cost_type' : row.get('cost_type', None),
                        'cpm' : row.get('cpm', None),
                        'impressions_limit' : row.get('impressions_limit', None),
                        'ad_platform' : row.get('ad_platform', None),
                        'ad_platform_no_wall' : row.get('ad_platform_no_wall', None),
                        'all_limit' : row.get('all_limit', None),
                        'category1_id' : row.get('category1_id', None),
                        'category2_id' : row.get('category2_id', None),
                        'status' : row.get('status', None),
                        'name': row.get('name', None),
                        'approved': row.get('approved', None),
                        'video': row.get('video', None),
                        'disclaimer_medical': row.get('disclaimer_medical', None),
                        'disclaimer_specialist': row.get('disclaimer_specialist', None),
                        'day_limit':row.get('day_limit'),
                        'start_time':row.get('start_time'),
                        'stop_time':row.get('stop_time'),
                        'create_time':row.get('create_time'),
                        'update_time':row.get('update_time'),
                        'age_restriction':row.get('age_restriction')
                }

    for acc in accounts:
        clients = ti.xcom_pull(task_ids='clients', key=acc['account_id'])
        ads_for_account = []
        print('acount {} with {} clients'.format(acc['account_id'], len(clients)))
        if clients:
            for client in clients:
                try:
                    response = api.ads.getAds(account_id=acc['account_id'], client_id=client['id'], include_deleted=0)
                    for row in response:
                        adss.append(get_ads_dict(row))
                        ads_for_account.append(row['id'])
                except ApiError as e:
                    if 'Flood control' in str(e):
                        time.sleep(5)
                        response = api.ads.getAds(account_id=acc['account_id'], client_id=client['id'], include_deleted=0)
                        for row in response:
                            adss.append(get_ads_dict(row))
                            ads_for_account.append(row['id'])
                    else:
                        print(e)
        else:
            try:
                response = api.ads.getAds(account_id=acc['account_id'], include_deleted=0)
                for row in response:
                    adss.append(get_ads_dict(row))
            except ApiError as e:
                if 'Flood control' in str(e):
                    time.sleep(5)
                    response = api.ads.getAds(account_id=acc['account_id'], include_deleted=0)
                    for row in response:
                        adss.append(get_ads_dict(row))
                print('error in ' + str(acc['account_id']))
        ti.xcom_push(key = str(acc['account_id']), value=ads_for_account)
    write_and_merge_info('vk_ads', (("ID", "NUMBER"), ("CAMPAIGN_ID", "NUMBER"), ("NAME", "VARCHAR2(200 CHAR)"), ("AD_FORMAT", "NUMBER"), \
        ("COST_TYPE", "NUMBER"), ("CPM", "NUMBER"), ("IMPRESSIONS_LIMIT", "NUMBER"), \
        ("AD_PLATFORM", "VARCHAR2(200 CHAR)"), ("AD_PLATFORM_NO_WALL", "VARCHAR2(200 CHAR)"), ("ALL_LIMIT", "NUMBER"), \
	    ("CATEGORY1_ID", "NUMBER"), ("CATEGORY2_ID", "NUMBER"), ("STATUS", "NUMBER"), ("APPROVED", "NUMBER"), ("DAY_LIMIT", "NUMBER"), \
	    ("START_TIME", "NUMBER"), ("STOP_TIME", "NUMBER"), ("CREATE_TIME", "NUMBER"), ("UPDATE_TIME", "NUMBER"), ("AGE_RESTRICTION", "NUMBER")), adss)


	
def vk_get_adsLayout(ds, **kwargs):
    ti = kwargs['ti']
    accounts = ti.xcom_pull(task_ids='acount')
    api = auth()
    adss = []
    def get_ads_dict(row):
        return {'ad_id':row.get('id'), 
        'client_id':row.get('client_id'),
        'campaign_id':row.get('campaign_id', None), 
		'age_restriction':row.get('age_restriction', None), 
		'title':row.get('title', None), 
		'link_url':row.get('link_url', None), 
		'link_domain':row.get('link_domain', None),
		'cost_type':row.get('cost_type', None), 
		'ad_format':row.get('ad_format', None), 
		'preview_link':row.get('preview_link', None), 
		'image_src':row.get('image_src', None),
		'stats_url':row.get('stats_url', None),
		'description':row.get('description', None)}
    for acc in accounts:
        clients = ti.xcom_pull(task_ids='clients', key=acc['account_id'])
        print('acount {} with {} clients'.format(acc['account_id'], len(clients)))
        if clients:
            for client in clients:
                try:
                    response = api.ads.getAdsLayout(account_id=acc['account_id'], client_id=client['id'], include_deleted=1)
                    [c.update({'client_id':client['id']}) for c in response]
                    for row in response:
                        adss.append(get_ads_dict(row))
                except ApiError:
                    time.sleep(5)
                    response = api.ads.getAdsLayout(account_id=acc['account_id'], client_id=client['id'], include_deleted=1)
                    [c.update({'client_id':client['id']}) for c in response]
                    for row in response:
                        adss.append(get_ads_dict(row))
        else:
            try:
                response = api.ads.getAdsLayout(account_id=acc['account_id'], include_deleted=1)
                [c.update({'client_id':''}) for c in response]
                for row in response:
                    adss.append(get_ads_dict(row))
            except:
                print('error in ' + str(acc['account_id']))
    write_and_merge_info('vk_ads_layout', (("AD_ID", "NUMBER"), ("CLIENT_ID", "NUMBER"), ("CAMPAIGN_ID", "NUMBER"), \
        ("AGE_RESTRICTION", "NUMBER"), ("TITLE", "VARCHAR2(400 CHAR)"), ("LINK_URL", "VARCHAR2(800 CHAR)"), ("LINK_DOMAIN", "VARCHAR2(800 CHAR)"), \
        ("COST_TYPE", "NUMBER"), ("AD_FORMAT", "NUMBER"), ("PREVIEW_LINK", "VARCHAR2(800 CHAR)"), ("IMAGE_SRC", "VARCHAR2(800 CHAR)"), \
        ("STATS_URL", "VARCHAR2(800 CHAR)"), ("DESCRIPTION", "VARCHAR2(800 CHAR)")), adss)
			

def vk_get_campaigns(ds, **kwargs):
    ti = kwargs['ti']
    accounts = ti.xcom_pull(task_ids='acount')
    api = auth()
    responces = []
    for acc in accounts:
        clients = ti.xcom_pull(task_ids='clients', key=acc['account_id'])
        print('acount {} with {} clients'.format(acc['account_id'], len(clients)))
        if clients:
            for client in clients:
                try:
                    campaigns = api.ads.getCampaigns(account_id=acc['account_id'], client_id=client['id'], include_deleted=1)
                except ApiError:
                    time.sleep(5)
                    campaigns = api.ads.getCampaigns(account_id=acc['account_id'], client_id=client['id'], include_deleted=1)
                if campaigns:
                    [c.update({'account_id':acc['account_id']}) for c in campaigns]
                    [c.update({'client_id':client['id']}) for c in campaigns]
                    for campaign in campaigns:
                        responces.append(campaign)
    write_and_merge_info('vk_campaigns', (('ID', 'NUMBER(38,0)'), ('TYPE', 'VARCHAR2(800 CHAR)'), ('NAME', 'VARCHAR2(800 CHAR)'),  \
        ('STATUS', 'NUMBER(38,0)'), ('DAY_LIMIT', 'VARCHAR2(800 CHAR)'), ('ALL_LIMIT', 'VARCHAR2(800 CHAR)'), 
        ('START_TIME', 'NUMBER(38,0)'), ('STOP_TIME', 'NUMBER(38,0)'), ('CREATE_TIME', 'NUMBER(38,0)'), ('UPDATE_TIME', 'NUMBER(38,0)'), 
        ('ACCOUNT_ID', 'NUMBER(38,0)'), ('CLIENT_ID', 'NUMBER(38,0)')), responces)
    return responces

def vk_get_postsreach(ds, **kwargs):
    ti = kwargs['ti']
    camps = ti.xcom_pull(task_ids='campaigns')
    api = auth()
    accounts = ti.xcom_pull(task_ids='acount')
    wrong_indexes = []
    responses = []
    def get_postsreach_dict(row):
        return {
                    "id" : row.get('id'),
					"account_id" : row.get('account_id'),
                    "type" : row.get('campaign'),
                    "day" : row.get('day'),
                    "reach_subscribers" : row.get('reach_subscribers'),
                    "reach_total" : row.get('reach_total'),
                    "links" : row.get('links'),
                    "to_group" : row.get('to_group'),
                    "join_group" : row.get('join_group'),
                    "report" : row.get('report'),
                    "hide" : row.get('hide'),
                    "unsubscribe" : row.get('unsubscribe'),
                    "pretty_cards_clicks" : row.get('pretty_cards_clicks'),
                    "unsubscribe": row.get('pretty_cards_clicks'),
                    "video_views_start": row.get('video_views_start'),
                    "video_views_3s": row.get('video_views_3s'),
                    "video_views_25p": row.get('video_views_25p'),
                    "video_views_50p": row.get('video_views_50p'),
                    "video_views_75p": row.get('video_views_75p'),
                    "video_views_100p": row.get('video_views_100p')
                }
    for acc in accounts:
        account_id = acc['account_id']
        camp_ids = [c['id']  for c in camps if c['account_id'] == account_id]
        for i in range(0,len(camp_ids), 3):
            try:
                response = api.ads.getPostsReach(account_id=account_id, ids_type='campaign', ids=camp_ids[i:i+3])
                [c.update({'account_id': acc['account_id'], 'type': 'campaign', 'day': datetime.strftime(datetime.today(), '%Y-%m-%d')}) for c in response]
                for row in response:
                    responses.append(get_postsreach_dict(row))

            except ApiError as e:
                if 'Flood control' in str(e):
                    time.sleep(5)
                    try:
                        response = api.ads.getPostsReach(account_id=account_id, ids_type='campaign', ids=camp_ids[i:i+3])
                        for row in response:
                            responses.append(get_postsreach_dict(row))
                    except ApiError as e:
                        print(e)
                elif 'ids[' in str(e):
                    index1 = str(e).index('ids[')
                    index2 = str(e).rindex(']')
                    index = str(e)[index1+4:index2]
                    wrong_indexes.append(camp_ids[int(index)])
    print(wrong_indexes)
    write_and_merge_info('vk_postsreach', (('ID', 'NUMBER(38,0)'), ('ACCOUNT_ID', 'NUMBER(38,0)'), ('DAY', 'DATE'),  \
        ('REACH_SUBSCRIBERS', 'NUMBER(38,0)'), ('REACH_TOTAL', 'NUMBER(38,0)'), ('LINKS', 'NUMBER(38,0)'), ('TO_GROUP', 'NUMBER(38,0)'), \
        ('JOIN_GROUP', 'NUMBER(38,0)'), ('REPORT', 'NUMBER(38,0)'), ('HIDE', 'NUMBER(38,0)'), ('UNSUBSCRIBE', 'NUMBER(38,0)'), ('VIDEO_VIEWS_START', 'NUMBER(38,0)'), \
		('VIDEO_VIEWS_3S', 'NUMBER(38,0)'), ('VIDEO_VIEWS_25P', 'NUMBER(38,0)'), ('VIDEO_VIEWS_50P', 'NUMBER(38,0)'), ('VIDEO_VIEWS_75P', 'NUMBER(38,0)'),  \
		('VIDEO_VIEWS_100P', 'NUMBER(38,0)'), ('TYPE', 'VARCHAR2(100 BYTE)')), responses)

def add_stats(i, ids_type, responses, api, account_id, camp_ids, get_statistic_dict):
            start_date = START_DATE if START_DATE else (datetime.today()-timedelta(days=5)).strftime("%Y-%m-%d")
            end_date = END_DATE if END_DATE else datetime.today().strftime("%Y-%m-%d")
            response = api.ads.getStatistics(account_id=account_id,ids_type = ids_type ,ids = camp_ids[i:i+10], 
                    period = 'day', date_from = start_date, date_to = end_date)
            [c.update({'account_id':account_id}) for c in response]
            for row in response:
                for stats in row['stats']:
                    responses.append(get_statistic_dict(row, stats))


def vk_campaigns_daily_stat(ds, **kwargs):
    def get_statistic_dict(row, stats):
        return {
            'campaign_id' : row.get('id'),
            'account_id' : row.get('account_id'),
            'day' : stats.get('day', None),
            'spent' : stats.get('spent', None),
            'impressions' : stats.get('impressions', None),
            'overall' : stats.get('overall', None),
            'clicks' : stats.get('clicks', None),
            'reach' : stats.get('reach', None),
            'video_views' : stats.get('video_views', None),
            'video_views_half' : stats.get('video_views_half', None),
            'video_views_full' : stats.get('video_views_full', None),
            'video_clicks_site' : stats.get('video_clicks_site', None),
            'join_rate' : stats.get('join_rate', None)
            }
    ti = kwargs['ti']
    accounts = ti.xcom_pull(task_ids='acount')
    camps = ti.xcom_pull(task_ids='campaigns')
    api = auth()
    responses = []
    for acc in accounts:
        account_id = acc['account_id']
        camp_ids = [c['id'] for c in camps if c['account_id'] == account_id]
        print('{} camps in account {}'.format(len(camp_ids), account_id))
        for i in range(0,len(camp_ids),10):
            try:
                add_stats(i, 'campaign', responses, api, account_id, camp_ids, get_statistic_dict)
            except ApiError as e:
                if 'Flood control' in str(e):
                    time.sleep(5)
                    add_stats(i, 'campaign', responses, api, account_id, camp_ids, get_statistic_dict)
                else:
                    print(e)

    write_and_merge_info('vk_campaign_daily_stat', (('CAMPAIGN_ID', 'NUMBER(38,0)'), ('ACCOUNT_ID', 'NUMBER(38,0)'), ('DAY', 'DATE'),  \
        ('SPENT', 'NUMBER(38,0)'), ('IMPRESSIONS', 'NUMBER(38,0)'), ('CLICKS', 'NUMBER(38,0)'), ('REACH', 'NUMBER(38,0)'), ('VIDEO_VIEWS', 'NUMBER(38,0)'), \
        ('VIDEO_VIEWS_HALF', 'NUMBER(38,0)'), ('VIDEO_CLICKS_SITE', 'NUMBER(38,0)'), ('JOIN_RATE', 'NUMBER(38,0)')), responses, 3)


def vk_ads_daily_stat(ds, **kwargs):
    def get_statistic_dict(row, stats):
        return {
            'account_id' : row.get('account_id'),
            'client_id' : row.get('id'),
            'day' : stats.get('day', None),
            'ad_id' : stats.get('ad_id', None),
            'spent' : stats.get('spent', None),
            'impressions' : stats.get('impressions', None),
            'overall' : stats.get('overall', None),
            'clicks' : stats.get('clicks', None),
            'reach' : stats.get('reach', None),
            'video_views' : stats.get('video_views', None),
            'video_views_half' : stats.get('video_views_half', None),
            'video_views_full' : stats.get('video_views_full', None),
            'video_clicks_site' : stats.get('video_clicks_site', None),
            'join_rate' : stats.get('join_rate', None)
            }
    ti = kwargs['ti']
    accounts = ti.xcom_pull(task_ids='acount')
    api = auth()
    responses = []
    for acc in accounts:
        account_id = acc['account_id']
        ads = ti.xcom_pull(task_ids='ads', key=account_id)
        if ads:
            print('{} ads in account {}'.format(len(ads), account_id))
            try:
                for i in range(0,len(ads),10):
                    add_stats(i, 'ad', responses, api, account_id, ads, get_statistic_dict)
            except ApiError as e:
                if 'Flood control' in str(e):
                    time.sleep(5)
                    response = api.ads.getStatistics(account_id=account_id,ids_type = 'ad' ,ids = list(map(int, ads[i:i+10])), 
                    period = 'day', date_from = (datetime.today()-timedelta(days=5)).strftime("%Y-%m-%d"), date_to = datetime.today().strftime("%Y-%m-%d"))
                    for row in response:
                        for stats in row['stats']:
                            responses.append(get_statistic_dict(row, stats))
                else:
                    print(e)
    write_and_merge_info('vk_ads_daily_stat', (('CLIENT_ID', 'NUMBER(38,0)'), ('ACCOUNT_ID', 'NUMBER(38,0)'), ('AD_ID', 'NUMBER(38,0)'), ('DAY', 'DATE'),  \
        ('SPENT', 'NUMBER(38,0)'), ('IMPRESSIONS', 'NUMBER(38,0)'), ('CLICKS', 'NUMBER(38,0)'), ('REACH', 'NUMBER(38,0)'), ('VIDEO_VIEWS', 'NUMBER(38,0)'), \
        ('VIDEO_VIEWS_HALF', 'NUMBER(38,0)'), ('VIDEO_CLICKS_SITE', 'NUMBER(38,0)'), ('JOIN_RATE', 'NUMBER(38,0)')), responses, 4)



default_args = {
    'owner': 'airflow',
    'depends_on_past': False,
    'start_date': utils.dates.days_ago(2),
	'provide_context': True,
	}

dag = DAG('vkapi', default_args=default_args)


vk_acount = PythonOperator(
   task_id='acount',
   python_callable=vk_get_acounts,
   provide_context=True,
   dag=dag
)

vk_clients = PythonOperator(
   task_id='clients',
   python_callable=vk_get_clients,
   provide_context=True,
   dag=dag
)

vk_campaigns = PythonOperator(
   task_id='campaigns',
   python_callable=vk_get_campaigns,
   provide_context=True,
   dag=dag
)


vk_ads = PythonOperator(
   task_id='ads',
   python_callable=vk_get_ads,
   provide_context=True,
   dag=dag
)


vk_adsLayout = PythonOperator(
   task_id='adsLayout',
   python_callable=vk_get_adsLayout,
   provide_context=True,
   dag=dag
)


vk_campaigns_daily = PythonOperator(
   task_id='statistics_camps',
   python_callable=vk_campaigns_daily_stat,
   provide_context=True,
   dag=dag
)

vk_ads_daily = PythonOperator(
   task_id='statistics_ads',
   python_callable=vk_ads_daily_stat,
   provide_context=True,
   dag=dag
)

vk_postsreach = PythonOperator(
   task_id='postsreach',
   python_callable=vk_get_postsreach,
   provide_context=True,
   dag=dag
)


[vk_ads_daily, vk_campaigns_daily] << vk_postsreach << [vk_adsLayout, vk_ads, vk_campaigns] << vk_clients << vk_acount