from facebook_business.adobjects.adaccount import AdAccount
from facebook_business.adobjects.adaccountuser import AdAccountUser
from facebook_business.api import FacebookAdsApi, FacebookRequestError

from airflow import DAG, utils
from airflow.operators.python_operator import PythonOperator

class FBAgency:
    app_id = '360179464556699'
    app_secret = 'eb5f37e7bae8b5c58fb3baa54db2ceed'
    access_token = 'EAAFHlNEKUJsBAK8ganyz4NNrQfTHfVUNIMfUx611lBRYC21yHwZALDXGEOjZCgAoTzYnSkO6elQp6fr07bZBjgcoDdQodkWr7ZAagZCdkbsWSmGjZCwWTyEZAbMZAAOmZCWUd7pWS3xL7TAMumQVvTHlNI88pQzQH3TIOztZCrQEsBu8wQKMd1rQNI'
   

	
class FacebookAPI:
    def __init__(self):
        self.agency = FBAgency()
        self.api = FacebookAdsApi.init(self.agency.app_id, self.agency.app_secret, self.agency.access_token)
        self.me = AdAccountUser(fbid='me')

    def get_accounts(self):
        ad_accounts_data = []
        ad_accounts = self.me.get_ad_accounts(fields=[
            AdAccount.Field.account_id,
            AdAccount.Field.name,
            AdAccount.Field.id,
            AdAccount.Field.currency,
            AdAccount.Field.balance,
            AdAccount.Field.amount_spent
        ])
        for acc in ad_accounts:
            ad_accounts_data.append(dict(acc))
        return ad_accounts_data

def fb_get_accounts(ds, **kwargs):
    api = FacebookAPI()
    return api.get_accounts()

		
default_args = {
    'owner': 'airflow',
    'depends_on_past': False,
    'start_date': utils.dates.days_ago(2),
	'provide_context': True,
	}

dag = DAG('fb_api', default_args=default_args)

fb_accounts = PythonOperator(
   task_id='accounts',
   python_callable=fb_get_accounts,
   provide_context=True,
   dag=dag
)

fb_accounts